#ifndef MAXEL_HPP
#define MAXEL_HPP

#include <SFML/Graphics.hpp>

class Maxel{
public:
	sf::RectangleShape rect;
	bool solid;
	
	Maxel(bool _solid = false);
	~Maxel();
};

#endif //MAXEL_HPP