#ifndef MAP_HPP
#define MAP_HPP

#include "Maxel.hpp"

class Map{
public:
	
	const static int HEIGHT = 600;
	const static int WIDTH = 1000;
	Maxel** map;
	
	Map();
	~Map();
	void draw(sf::RenderWindow&);
};



#endif //MAP_HPP