#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include "headers/Map.hpp"

//> DOXYGEN DOC "how to"-> http://www.stack.nl/~dimitri/doxygen/manual/docblocks.html
//> Tous les pro tips pour pouvoir bien commenter notre code <3

/**
 * @brief Main function of our project
 * @return SUCCESS or not
 */
 
int main()
{
	unsigned int height = 720;
	unsigned int width = 1200;
	
	std::string title = "asticot - 0.0.1.alpha";
	
	sf::VideoMode videoMode(width, height);
	sf::RenderWindow window(videoMode, title);
	
	Map map;

	while (window.isOpen()){
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		
		window.clear();
		map.draw(window);
		window.display();
	}

	return 0;
}
