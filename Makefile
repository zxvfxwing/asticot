.PHONY: clean All

All:
	@echo "----------Building project:[ the_asticot - Debug ]----------"
	@cd "the_asticot" && "$(MAKE)" -f  "the_asticot.mk"
clean:
	@echo "----------Cleaning project:[ the_asticot - Debug ]----------"
	@cd "the_asticot" && "$(MAKE)" -f  "the_asticot.mk" clean
