#include "../headers/Map.hpp"
#include <math.h>

Map::Map(){
	map = new Maxel*[Map::HEIGHT];
	for(int i=0; i<Map::HEIGHT; ++i){
		map[i] = new Maxel[Map::WIDTH];
		for(int j=0; j<Map::WIDTH; ++j){
			map[i][j].rect.setPosition((float)j,(float)i);
			
		}
	}
	for(int j=0; j<Map::WIDTH; ++j)
		for(int i=0; i<Map::HEIGHT; ++i)
			if((i-(float)Map::HEIGHT/2)>=sin((float)j/60)*80){
				map[i][j].rect.setFillColor(sf::Color(97, 63, 16));
				map[i][j].solid = true;
			}
}

Map::~Map(){
	for(int i=0; i<Map::HEIGHT; ++i)
		delete[] map[i];
	delete[] map;
}

void Map::draw(sf::RenderWindow& window){
	for(int i=0; i<Map::HEIGHT; ++i)
		for(int j=0; j<Map::WIDTH; ++j)
			window.draw(map[i][j].rect);
}